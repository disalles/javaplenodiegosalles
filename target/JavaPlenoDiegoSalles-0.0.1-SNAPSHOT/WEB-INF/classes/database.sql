CREATE TABLE `Departamento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(60) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `Empregado` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(30) DEFAULT NULL,
  `Endereco` varchar(60) DEFAULT NULL,
  `Matricula` int(11) DEFAULT NULL,
  `TipoCargo` varchar(10) DEFAULT NULL,
  `IdDepartamento` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY(`IdDepartamento`) REFERENCES Departamento(`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO Departamento (Nome) VALUES ("Financas");
INSERT INTO `javaplenodiego_test`.`Empregado`
(TipoCargo,
Endereco,
Nome,
Matricula,
IdDepartamento)
VALUES
("PLENO",
"Rua l",
"Paulo",
"123455",
null);
