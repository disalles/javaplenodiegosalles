<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<c:url value="/static/css/empregados" var="cssPath" />
<link rel="stylesheet" href="${cssPath}/message.css"/>
<tags:pageTemplate titulo="Java Pleno Diego Salles">

	<div class="cadastro col-md-8 col-md-offset-2">
		<h1>Cadastro de Empregados</h1>
		<form:form action="${s:mvcUrl('EC#gravar').build() }" method="post"
			commandName="empregado" enctype="multipart/form-data">
			
			<form:hidden  path="id" />
			<div class="form-group">
				<label>Nome</label>
				<form:input cssClass="form-control" path="nome"
				title="${empregado.nome}" />
				<form:errors cssStyle="message" path="nome" />
			</div>
			<div class="form-group">
				<label>Endereco</label>
				<form:input cssClass="form-control" path="endereco" 
				title="${empregado.endereco}"/>
				<form:errors path="endereco" />
			</div>
			<div class="form-group">
				<label>Numero Matricula</label>
				<form:input cssClass="form-control" path="numeroMatricula"
				title="${empregado.numeroMatricula}" />
				<form:errors path="numeroMatricula" />
			</div>
				<div class="form-group">
					<label>Cargo</label>
					<form:input cssClass="form-control"
						path="cargo" title="${empregado.cargo}" />
					<form:errors path="cargo" />	
				</div>
				<div class="form-group">
					<label>Departamento</label>
					<form:hidden path="departamento.id"/>
					<form:input cssClass="form-control" path="departamento.nomeDepartamento"/>
					<form:errors  path="departamento.nomeDepartamento" />
				</div>
			<button class="btn btn-default" type="submit">Cadastrar</button>
		</form:form>
	</div>
</tags:pageTemplate>