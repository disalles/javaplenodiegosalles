CREATE TABLE `Departamento` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(60) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `Empregado` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(30) NOT NULL,
  `Endereco` varchar(60) NOT NULL,
  `Matricula` int(11) NOT NULL,
  `TipoCargo` varchar(10) NOT NULL,
  `IdDepartamento` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY(`IdDepartamento`) REFERENCES Departamento(`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO Departamento (Nome) VALUES ("Financas");
INSERT INTO Empregado(TipoCargo,Endereco,Nome,Matricula,IdDepartamento)VALUES("PLENO","Rua l","Paulo","123455",1);
