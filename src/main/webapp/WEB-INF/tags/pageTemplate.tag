<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="titulo" required="true"%>
<%@ attribute name="bodyClass" required="false"%>
<%@ attribute name="extraScripts" fragment="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!doctype html>
<html>
<head>
<title>${titulo }- Chanel Tometoes</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<c:url value="/static/css" var="cssPath" />
<c:url value="/static/js" var="jsPath" />
<link rel="stylesheet" href="${cssPath}/bootstrap.min.css" />
<link rel="stylesheet" href="${cssPath}/bootstrap-theme.min.css" />
<link rel="stylesheet" href="${cssPath}/sticky-footer.css"/>
</head>
<body class="${bodyClass}">

	<%@include file="/WEB-INF/pages/fragments/header.jsp" %>
	<jsp:doBody />

     <%@include file="/WEB-INF/pages/fragments/footer.jsp" %> 

	<jsp:invoke fragment="extraScripts" />
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>	
<script src="${jsPath}/bootstrap.min.js"></script>

</body>
</html>








