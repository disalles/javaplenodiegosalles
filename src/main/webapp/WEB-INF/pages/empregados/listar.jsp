<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<c:url value="/static/imagem" var="imagemPath" />
<c:url value="/static/css" var="cssPath" />
<link rel="stylesheet" href="${cssPath}/messages.css"/>
<tags:pageTemplate titulo="Java Pleno Diego Salles">
	<div class="container">
		<h1>Empregados</h1>
		<p>${message}</p>
		<table  class="table table-bordered table-striped table-hover messages">
			<tr> 
				<th >Nome</th>
				<th>Endereco</th>
				<th>Departamento</th>
				<th>Numero da Matricula</th>
				<th>Remover</th>
			</tr>
			<c:forEach items="${empregados}" var="empregado">
				<tr align="center" >
					<td><a
						href="${s:mvcUrl('EC#detalhe').arg(0,empregado.id).build() }">${empregado.nome}</a>
					</td>
					<td>${empregado.endereco}</td>

					<td>${empregado.departamento.nomeDepartamento}</td>

					<td>${empregado.numeroMatricula}</td>
					<td><a href="${s:mvcUrl('EC#remover').arg(0,empregado.id).build()}" id="logo">
							<img height="16" width="16" src="${imagemPath}/close.png" />
					</a></td>

				</tr>
			</c:forEach>
		</table>
		
		<div class="col-md-offset-9">
					<a href="/empregados/cadastrar">
						Cadastrar Usuario
					</a>
		  <p></p>
		</div>
	</div>
</tags:pageTemplate>



















