<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<c:url value="/static/imagem" var="imagemPath" />
<tags:pageTemplate titulo="Java Pleno Diego Salles">
	<div class="container">
		<h1>Departamentos</h1>
		<p></p>
		<table   class="table table-bordered table-striped table-hover">
			<tr>
				<th>Nome Departamento</th>
				<th>Remover</th>
			</tr>
			<c:forEach items="${departamentos}" var="departamento">
				<tr align="center" >
					<td>${departamento.nomeDepartamento}</td>
					<td><a href="${s:mvcUrl('DC#remover').arg(0,departamento.id).build()}" id="logo">
							<img height="16" width="16" src="${imagemPath}/close.png" />
					</a></td>

				</tr>
			</c:forEach>
		</table>
		<div>
		<h2>Total de departamentos sem empregados ${total}</h2>
		</div>
	</div>
</tags:pageTemplate>



















