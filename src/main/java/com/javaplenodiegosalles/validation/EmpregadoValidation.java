package com.javaplenodiegosalles.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.javaplenodiegosalles.models.Empregado;


public class EmpregadoValidation implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return Empregado.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(errors, "nome", "field.required.empregado.nome");
		ValidationUtils.rejectIfEmpty(errors, "departamento.nomeDepartamento", "field.required.empregado.departamento.nome");
		ValidationUtils.rejectIfEmpty(errors, "numeroMatricula", "field.required.empregado.matricula");
		ValidationUtils.rejectIfEmpty(errors, "cargo", "field.required.empregado.cargo");
		ValidationUtils.rejectIfEmpty(errors, "endereco", "field.required.empregado.endereco");
	}
}
