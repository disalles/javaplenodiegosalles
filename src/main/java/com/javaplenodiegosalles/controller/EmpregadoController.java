package com.javaplenodiegosalles.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.javaplenodiegosalles.models.Departamento;
import com.javaplenodiegosalles.models.Empregado;
import com.javaplenodiegosalles.repositorios.RepositorioDepartamento;
import com.javaplenodiegosalles.repositorios.RepositorioEmpregado;
import com.javaplenodiegosalles.validation.EmpregadoValidation;



@Controller
@RequestMapping("/empregados")
public class EmpregadoController {

	@Autowired
	private RepositorioEmpregado repositorioEmpregado;
	
	@Autowired
	private RepositorioDepartamento repositorioDepartamento;
	
	
	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		binder.addValidators(new EmpregadoValidation());
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@Cacheable(value="empregados")
	public ModelAndView index() 
	{
		repositorioEmpregado.setClazze(Empregado.class);
		List<Empregado> empregados = (List<Empregado>) repositorioEmpregado.findAll();
		ModelAndView modelAndView = new ModelAndView("empregados/listar");
		modelAndView.addObject("empregados", empregados);
		return modelAndView;
	}

	@RequestMapping("/detalhe/{id}")
	public ModelAndView detalhe(@PathVariable("id") Integer id) {
		repositorioEmpregado.setClazze(Empregado.class);
		ModelAndView modelAndView = new ModelAndView("empregados/form");
		Empregado empregadoBd = repositorioEmpregado.findOne(id);
		modelAndView.addObject("empregado", empregadoBd);
		return modelAndView;
	}
	
	@RequestMapping("/form")
	public ModelAndView form(Empregado empregado) {
		ModelAndView modelAndView = new ModelAndView("empregados/form");
		modelAndView.addObject("empregado",empregado);
		return modelAndView;
	}
	
	@RequestMapping("/cadastrar")
	public ModelAndView cadastra()
	{
		ModelAndView modelAndView = new ModelAndView("empregados/form");
		modelAndView.addObject("empregado",new Empregado());
		return modelAndView;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@CacheEvict(value={"empregados","departamentos"}, allEntries=true)
	public ModelAndView  gravar(@Valid Empregado empregado,BindingResult result,
			RedirectAttributes redirectAttributes)
	{
		if (result.hasErrors()) {
			return form(empregado);
		}
		
		repositorioEmpregado.setClazze(Empregado.class);
		Departamento departamento = repositorioDepartamento.save(empregado.getDepartamento());
		empregado.setDepartamento(departamento);
		repositorioEmpregado.save(empregado);
		redirectAttributes.addFlashAttribute("message", "Empregado cadastrado ou atualizado com sucesso");
		return new ModelAndView("redirect:");
	}
	
	@RequestMapping("/remover/{id}")
	@CacheEvict(value={"empregados","departamentos"}, allEntries=true)
	public ModelAndView remover(@PathVariable("id") Integer id) {
		repositorioEmpregado.setClazze(Empregado.class);
		Empregado empregado = repositorioEmpregado.findOne(id);
		repositorioEmpregado.delete(empregado);
		return new ModelAndView("redirect:/empregados");
	}
}
