package com.javaplenodiegosalles.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.javaplenodiegosalles.models.Departamento;
import com.javaplenodiegosalles.models.Empregado;
import com.javaplenodiegosalles.repositorios.RepositorioDepartamento;

@Controller
@RequestMapping("/departamentos")
public class DepartamentoController {
	
	@Autowired
	private RepositorioDepartamento repositorioDepartamento;
	
	
	@RequestMapping(method = RequestMethod.GET)
	@Cacheable(value="departamentos")
	public ModelAndView index() 
	{
		repositorioDepartamento.setClazze(Departamento.class);
		List<Departamento> departamentos = (List<Departamento>) repositorioDepartamento.findAll();
		int total = repositorioDepartamento.totalDepartamentoSemEmpregados();
		ModelAndView modelAndView = new ModelAndView("departamentos/listar");
		modelAndView.addObject("departamentos", departamentos);
		modelAndView.addObject("total", total);
		return modelAndView;
	}

	@RequestMapping("/remover/{id}")
	@CacheEvict(value={"departamentos"}, allEntries=true)
	public ModelAndView remover(@PathVariable("id") Integer id) {
		Departamento departamento = repositorioDepartamento.findOne(id);
		departamento.setEmpregados(new ArrayList<Empregado>());
		repositorioDepartamento.save(departamento);
		repositorioDepartamento.delete(departamento);
		return new ModelAndView("redirect:/departamentos");
	}

}
