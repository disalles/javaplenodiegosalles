package com.javaplenodiegosalles.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value=DataIntegrityViolationException.class)
	public ModelAndView constraintViolation(Exception exception) {
		ModelAndView modelAndView = new ModelAndView("error");
		return modelAndView;
	}
}