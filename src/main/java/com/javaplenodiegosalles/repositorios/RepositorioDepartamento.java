package com.javaplenodiegosalles.repositorios;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.javaplenodiegosalles.models.Departamento;


@Repository
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class RepositorioDepartamento extends AbstractRepository<Departamento> {

	public int totalDepartamentoSemEmpregados() {
		CriteriaBuilder criteriaBuilder = getManager().getCriteriaBuilder();
		CriteriaQuery<Departamento> criteriaQuery = criteriaBuilder
				.createQuery(Departamento.class);
		Root<Departamento> fromDepartamento = criteriaQuery.from(Departamento.class);
		criteriaQuery.select(fromDepartamento).where(criteriaBuilder.isEmpty(fromDepartamento.get("empregados")));
		Query query = getManager().createQuery(criteriaQuery);

		return  query.getResultList().size();
	}

}
