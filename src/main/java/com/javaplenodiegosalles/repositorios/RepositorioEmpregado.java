package com.javaplenodiegosalles.repositorios;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import com.javaplenodiegosalles.models.Empregado;

@Repository
@Scope( proxyMode = ScopedProxyMode.TARGET_CLASS )
public class RepositorioEmpregado extends AbstractRepository <Empregado>{
	
	
	
}