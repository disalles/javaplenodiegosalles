package com.javaplenodiegosalles.repositorios;

public interface IRepository<T> {
	public <S extends T> S save(S entity);

    public T findOne(Integer id);

	public Iterable<T> findAll();

    public Long count();

    public void delete(T entity);

	public boolean exists(Integer id);
}
