package com.javaplenodiegosalles.repositorios;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public class AbstractRepository<T> implements IRepository<T> {

	@PersistenceContext
	private EntityManager manager;

	@Autowired(required = false)
	private Class<T> clazze;

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	@Override
	public <S extends T> S save(S entity) {
		return manager.merge(entity);
	}

	@Override
	public T findOne(Integer id) {
		return manager.find(clazze, id);
	}

	public Iterable<T> findPaginate(int valorInicial, int maxPorPagina) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazze);
		criteriaQuery.select(criteriaQuery.from(clazze));
		Query query = manager.createQuery(criteriaQuery);
		query.setFirstResult(valorInicial);
		query.setMaxResults(maxPorPagina);
		return query.getResultList();
	}

	@Override
	public Long count() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<T> fromAlcadas = criteriaQuery.from(clazze);
		criteriaQuery.select(criteriaBuilder.count(fromAlcadas));
		Query query = manager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}

	@Override
	public void delete(T entity) {
		manager.remove(entity);

	}

	@Override
	public boolean exists(Integer id) {
		return manager.contains(id);
	}

	@Override
	public List<T> findAll() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazze);
		criteriaQuery.select(criteriaQuery.from(clazze));
		Query query = manager.createQuery(criteriaQuery);
		return query.getResultList();
	}

	public void setClazze(Class<T> clazze) {
		this.clazze = clazze;
	}

}
