package com.javaplenodiegosalles.builder;

import java.util.ArrayList;
import java.util.List;

import com.javaplenodiegosalles.models.Departamento;

public class DepartamentoBuilder {

	private List<Departamento> departamentos = new ArrayList<>();

	private DepartamentoBuilder(Departamento departamento) {
		departamentos.add(departamento);
	}

	public static DepartamentoBuilder newDepartamento() {
		Departamento departamento = create("Software Engineering");
		return new DepartamentoBuilder(departamento);
	}
	

	private static Departamento create(String nomeDepartamento) {
		Departamento departamento = new Departamento();
		departamento.setNomeDepartamento(nomeDepartamento);
		return departamento;
	}
	
	
	public DepartamentoBuilder more(int number) {
        for (int i = 0; i < number; i++) {
            departamentos.add(create("Desenvolvimento"+i));
        }
        return this;
    }

	public Departamento buildOne() {
		return departamentos.get(0);
	}

	public List<Departamento> buildAll() {
		return departamentos;
	}
}
