package com.javaplenodiegosalles.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.javaplenodiegosalles.models.Departamento;
import com.javaplenodiegosalles.models.Empregado;
import com.javaplenodiegosalles.models.TipoCargo;

public class EmpregadoBuilder {

    private List<Empregado> empregados = new ArrayList<>();

    private EmpregadoBuilder(Empregado empregado) {
        empregados.add(empregado);
    }
    
    private EmpregadoBuilder() {
		
	}

    public static EmpregadoBuilder newEmpregado() {
        return new EmpregadoBuilder();
    }
    
    public static EmpregadoBuilder newEmpregado(String nome,String endereco,int matricula,Departamento departamento) {
        Empregado empregado = create(nome,endereco,matricula,TipoCargo.PLENO,departamento);
        return new EmpregadoBuilder(empregado);
    }

    private static Empregado create(String nome,String endereco,int numeroMatricula,TipoCargo cargo,Departamento departamento) {
        Empregado empregado = new Empregado();
        empregado.setNome(nome);
        empregado.setEndereco(endereco);
        empregado.setNumeroMatricula(numeroMatricula);
        empregado.setCargo(cargo);
        empregado.setDepartamento(departamento);
        return empregado;
    }

    
    public EmpregadoBuilder more(int number,Departamento departamento) {
        for (int i = 0; i < number; i++) {
            empregados.add(create("Individuo"+i,"rua"+i,new Random().nextInt(30000),TipoCargo.PLENO,departamento));
        }
        return this;
    }
    public Empregado buildOne() {
        return empregados.get(0);
    }

    public List<Empregado> buildAll() {
        return empregados;
    }
}