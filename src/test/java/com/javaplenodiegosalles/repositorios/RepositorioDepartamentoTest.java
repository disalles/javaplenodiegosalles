package com.javaplenodiegosalles.repositorios;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.javaplenodiegosalles.builder.DepartamentoBuilder;
import com.javaplenodiegosalles.builder.EmpregadoBuilder;
import com.javaplenodiegosalles.conf.DataSourceConfigurationTest;
import com.javaplenodiegosalles.conf.JPAConfiguration;
import com.javaplenodiegosalles.models.Departamento;
import com.javaplenodiegosalles.models.Empregado;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {JPAConfiguration.class,RepositorioDepartamento.class, DataSourceConfigurationTest.class})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")	
public class RepositorioDepartamentoTest {

	@Autowired
	private RepositorioDepartamento repositorioDepartamento;

	
	@Before 
	public void init()
	{
		repositorioDepartamento.setClazze(Departamento.class);
	}
	
	@Test 
	public void deveRetornarTotalDePartamentosSemEmpregados(){
		List<Empregado> empregados = new ArrayList<>();
		List<Departamento> departamentos = DepartamentoBuilder.newDepartamento().more(5).buildAll();

		for(Departamento departamento :departamentos )
		{
			departamento.setEmpregados(EmpregadoBuilder.newEmpregado().more(5, departamento).buildAll());
		}
		
		Departamento departamentoVazio = DepartamentoBuilder.newDepartamento().buildOne();
		departamentoVazio.setEmpregados(empregados);
		departamentos.stream().forEach(repositorioDepartamento::save);
		repositorioDepartamento.save(departamentoVazio);
		
		Assert.assertEquals(repositorioDepartamento.totalDepartamentoSemEmpregados(),1);
	}
}
