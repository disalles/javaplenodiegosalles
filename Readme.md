
Esse projeto foi contruido usando o framework Spring Boot

O Spring Boot iniciando Aplication como JavaApplication
O Tomcat ja vem configurado nele por default 
Esse projeto usa alem de Spring Boot o Spring Test
O teste é clean executa obtem o resultado e limpa o banco ele esta configurado
no datasource com profile test que pode ser visto no DataSourceConfigurationTest.java
Outras Tecnologias usadas JPA,Cache com Guava, Injecao de Dependencia do Spring.Vale salientar 
que guava não é a melhor estrategia o certo seria o uso de MemoryCache mas nesse exemplo pequeno Guava supri a necessidade

Configuração do banco de dados 
no diretorio resources se encontram alguns scripts basicos para criação do database.sql


Usabilidade
O Sitema roda 9000

http://localhost:9000/empregados/

Lista os empregados

Ao clicar no nome o usuario podera atualizar o empregado

As opções Deletar e Cadastrar ficam em http://localhost:9000/empregados/

Departamento

http://localhost:9000/Departamentos/

Voce podera excluir um departamento
e os departamentos serao listados
Não podera excluir um departamento que ja esteja vinculado a um empregado




